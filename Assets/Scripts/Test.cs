﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour {

	// Use this for initialization
	void Start () {
		APIManager.sInstance.SetUserInventory(3, EItems.dabalooney, -1, SetInventoryCallback);
	}

	private void SetInventoryCallback(InventoryChange change_){
		Debug.Log(change_.ToString());
	}

    private void UserInfoCallback(UserInfo userInfo_){
		Debug.Log(userInfo_.ToString());
	}

	private void UserInventoryCallback(Inventory inventory_){
		Debug.Log(inventory_.ToString());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
