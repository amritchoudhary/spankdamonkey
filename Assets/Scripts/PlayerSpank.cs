﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using TBM;

public class PlayerSpank : Player
{

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    [ClientRpc]
    public override void RpcAskReady() {
        if (isLocalPlayer) {
            GUIManager.instance.ShowReadyCanvas(true);
            GUIManager.instance.readyButton.onClick.AddListener(AskReadyListener);
        }
    }

    protected override void AskReadyListener() {
        GUIManager.instance.readyButton.onClick.RemoveAllListeners();
        GUIManager.instance.ShowReadyCanvas(false);

        _playerInfo._ifPlayerReady = true;
        UpdatePlayerInfo();
    }

    [ClientRpc]
    protected override void RpcGetTurn() {
        if (isLocalPlayer) {
            GUIManager.instance.StartTurn(TurnOverListener, _commonData._value, this);
        }
    }

    protected override void TurnOverListener(int value_) {
        turnReturnData = new TurnReturnData();
        turnReturnData._value = value_;
        string turnReturnDataString = JsonUtility.ToJson(turnReturnData);
        CmdTurnPlayed(turnReturnDataString);
    }


}
