﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    public static AudioManager instance;

    void Awake() {
        instance = this;
    }

    public AudioSource _audioSource;
    public List<AudioClip> _spankClips;
    public List<AudioClip> _giftClips;
    public AudioClip _strikeClip;
    public AudioClip _defeatClip;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void PlaySpank() {
        int rand = Random.Range(0, _spankClips.Count);
        _audioSource.PlayOneShot(_spankClips[rand]);
    }

    public void PlayGift() {
        int rand = Random.Range(0, _giftClips.Count);
        _audioSource.PlayOneShot(_giftClips[rand]);
    }

    public void PlayStrike() {
        _audioSource.PlayOneShot(_strikeClip);
    }

    public void PlayDefeat() {
        _audioSource.PlayOneShot(_defeatClip);
    }

}
