﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceRoll : MonoBehaviour
{

    public float rotationSpeed;
    public float slerpFactor;
    public float rotChangeTime;
    public List<Vector3> faceRotations;
    private Quaternion targetRotation;
    private Vector3 targetAxis;
    private float timer = 0;
    public bool isRolling = true;

    public void Roll() {
        transform.rotation = Quaternion.Euler(faceRotations[5]);
        isRolling = true;
    }

    public void Init() {

    }

    public void StopRoll(int result_) {
        isRolling = false;
        targetRotation = Quaternion.Euler(faceRotations[result_]);
    }

    private void Update() {
        if (isRolling) {
            timer += Time.deltaTime;

            if (timer > rotChangeTime) {
                targetAxis = Random.insideUnitSphere;
                timer = 0;
            } else {
                transform.Rotate(targetAxis, rotationSpeed * Time.deltaTime);
            }
        } else {
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, slerpFactor);
        }
    }

}
