﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ServerManager : NetworkBehaviour
{

    public List<LocalTurnHandler> players = new List<LocalTurnHandler>();
    public List<bool> ifPlayersReady = new List<bool>();
    public List<int> scores = new List<int>();
    public int whoseTurn = 0;


    public int syncInt = 0;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        //GUIManager.instance.ShowServerStatusText("var: " + syncInt);
        if (Input.GetKeyDown(KeyCode.S)) {
            syncInt++;
            UpdateClients(syncInt);
        }

        if (Input.GetKeyDown(KeyCode.P)){
            Init();
        }

        if (Input.GetKeyDown(KeyCode.L)) {
            if (IfAllPlayersReady())
                GiveTurn();
            else
                Debug.Log("Not All player ready");
        }
    }

    private void UpdateClients(int syncInt_) {
        foreach (LocalTurnHandler player in players) {
            player.UpdateClient(syncInt_);
        }
    }

    public void NewPlayer(LocalTurnHandler player_) {
        players.Add(player_);
        ifPlayersReady.Add(false);
        scores.Add(0);
    }

    public void Init() {
        InitPlayers();
    }

    public void InitPlayers() {
        for(int i = 0; i < players.Count; i++) {
            players[i].Init(this, i);
        }
    }

    public void RemoveLeftClients() {
        players.RemoveAll(X => X == null);
    }

    public void UpdateFromClient() {
        syncInt--;
        UpdateClients(syncInt);
    }

    public void GiveTurn() {
        players[whoseTurn].GetTurn();
    }

    public void TurnPlayed(int playerIndex, int turnResponse) {
        Debug.Log("Index: " + playerIndex + "played :" + turnResponse);
        players[playerIndex].score += turnResponse;
        whoseTurn++;
        if(whoseTurn >= players.Count) {
            whoseTurn = 0;
        }
        GiveTurn();
    }

    private bool IfAllPlayersReady() {
        for(int i =0; i < ifPlayersReady.Count; i++) {
            if (ifPlayersReady[i])
                continue;
            else
                return false;
        }
        return true;
    }

}
