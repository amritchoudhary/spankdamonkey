﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using TBM;

public class GUIManager : MonoBehaviour
{

    public static GUIManager instance;

    private Player mPlayer;
    public Text debugText;
    public Text statusText;
    public Text serverStatusText;
    public GameObject readyCanvasGO;
    public Button readyButton;
    public GameObject turnCanvasGO;
    public InputField inputField;
    public Button turnButton;
    private UnityAction<int> mTurnOverListener;
    public Button rollDiceButton;
    public DiceRoll diceRoll;
    public List<Sprite> monkeyImages;
    public Image monkeyImageHolder;
    public Image monkeyBarImageHolder;
    public Image monkeyGiftImageHolder;
    public GameObject spankCanvas;
    public Button spankButton;
    public Button spankFinishTurnButton;
    public Image handMark;
    public float handPositionMax;
    public float handMarkDuration;
    private int spanksRemaining = 0;
    public GameObject treasureCanvas;
    public Text treasureResultText;
    public Button useKeyButton;
    public GameObject keyImage;
    public GameObject giftCanvas;
    public Text giftRemainingText;
    private int giftsRemaining = 0;
    public Button giveGiftButton;
    public Button giveGiftButtonInSpankPanel;
    public Image giftMark;
    public Text giftRemainingTextGUI;
    public Text title;
    public GameObject happinessMeter;
    public Image happinessFG;
    public float factor;
    private int mCurrentHappiness;
    private int rollResult;
    public GameObject strikeCanvas;
    public Image strikeImage;
    public List<Sprite> strikeSprites;
    public GameObject defeatCanvas;
    public GameObject winCanvas;
    public Text coinsText;
    public Text aiPlayText;
    public float aiPlayShowDelay;
    private int turnTotalDelta = 0;
    private bool playerLost = false;

    void Awake() {
        instance = this;
    }

    // Use this for initialization
    void Start() {

    }

    public void GoToMainMenu() {
        SceneManager.LoadScene("MainMenu");
    }

    // Update is called once per frame
    void Update() {

    }

    public void ShowReadyCanvas(bool ifShow) {
        readyCanvasGO.SetActive(ifShow);
    }

    public void ShowText(string text_) {
        debugText.text = text_;
    }

    public void ShowStatusText(string text_) {
        statusText.text = text_;
    }

    public void ShowServerStatusText(string text_) {
        serverStatusText.text = text_;
    }

    public void StartTurn(UnityAction<int> turnOverListener_, int currentHappiness_, Player player_) {
        mPlayer = player_;
        happinessMeter.SetActive(true);
        mCurrentHappiness = currentHappiness_;
        mTurnOverListener = turnOverListener_;
        turnCanvasGO.SetActive(true);
        rollDiceButton.interactable = true;
        happinessFG.rectTransform.sizeDelta = new Vector2(600 * currentHappiness_ * factor, 40);
        monkeyImageHolder.sprite = monkeyImages[(int)((currentHappiness_ / 100.0f) * 6)];
        monkeyBarImageHolder.sprite = monkeyImages[(int)((currentHappiness_ / 100.0f) * 6)];
        turnTotalDelta = 0;
    }

    public void RollDice() {
        StartCoroutine(RollAnimation());
    }

    IEnumerator RollAnimation() {
        rollDiceButton.interactable = false;
        diceRoll.gameObject.SetActive(true);
        diceRoll.Roll();

        yield return new WaitForSeconds(5);

        rollResult = Random.Range(1, 7);
        spanksRemaining = rollResult;
        diceRoll.StopRoll(rollResult - 1);

        yield return new WaitForSeconds(2.0f);
        turnCanvasGO.SetActive(false);
        diceRoll.gameObject.SetActive(false);

        if (rollResult != 6) {
            spankCanvas.SetActive(true);
            spankButton.gameObject.SetActive(true);
            spankButton.GetComponentInChildren<Text>().text = "Spank (" + spanksRemaining + ")";

            if (giftsRemaining == 0) {
                giveGiftButtonInSpankPanel.gameObject.SetActive(false);
            } else {
                giveGiftButtonInSpankPanel.gameObject.SetActive(true);
                giftRemainingText.text = "Gift (" + giftsRemaining + ")";
            }
        } else {
            treasureCanvas.SetActive(true);
            keyImage.SetActive(true);
            treasureResultText.gameObject.SetActive(false);
            useKeyButton.interactable = true;

        }
    }

    public void Spank() {
        spanksRemaining--;
        mCurrentHappiness += 5;
        turnTotalDelta++;

        spankButton.interactable = false;
        handMark.rectTransform.localPosition = new Vector2(Random.Range(-handPositionMax, handPositionMax), Random.Range(-handPositionMax, handPositionMax));
        handMark.gameObject.SetActive(true);
        AudioManager.instance.PlaySpank();
        Invoke("TurnOffHandMark", handMarkDuration);

        if (mCurrentHappiness >= 100) {
            spankCanvas.SetActive(false);
            PlayerStrike();
            return;
        }

        happinessFG.rectTransform.sizeDelta = new Vector2(600 * mCurrentHappiness * factor, 40);
        monkeyImageHolder.sprite = monkeyImages[(int)((mCurrentHappiness / 100.0f) * 6)];
        monkeyBarImageHolder.sprite = monkeyImages[(int)((mCurrentHappiness / 100.0f) * 6)];

        if (spanksRemaining <= 0) {
            spankButton.gameObject.SetActive(false);
            spankFinishTurnButton.gameObject.SetActive(true);
        }
    }

    public void TurnOffHandMark() {
        handMark.gameObject.SetActive(false);
        spankButton.GetComponentInChildren<Text>().text = "Spank (" + spanksRemaining + ")";
        spankButton.interactable = true;
    }

    public void UseKeyOnTreasure() {
        int rand = Random.Range(1, 6);
        giftsRemaining += rand;
        keyImage.SetActive(false);
        treasureResultText.text = "You Got " + rand.ToString() + " Coins";
        treasureResultText.gameObject.SetActive(true);
        giftRemainingText.text = "Gift (" + giftsRemaining + ")";
        giftRemainingTextGUI.text = giftsRemaining.ToString();
        useKeyButton.interactable = false;
        StartCoroutine(StartGiveGift());
    }

    IEnumerator StartGiveGift() {
        yield return new WaitForSeconds(3);
        treasureCanvas.SetActive(false);
        giftCanvas.SetActive(true);
    }

    public void GiveGift() {
        giftsRemaining--;
        mCurrentHappiness -= 5;
        turnTotalDelta--;

        if (mCurrentHappiness < 0)
            mCurrentHappiness = 0;

        giveGiftButton.interactable = false;
        giftMark.rectTransform.localPosition = new Vector2(Random.Range(-handPositionMax, handPositionMax), Random.Range(-handPositionMax, handPositionMax));
        giftMark.gameObject.SetActive(true);
        AudioManager.instance.PlayGift();
        Invoke("TurnOffGiftMark", handMarkDuration);

        happinessFG.rectTransform.sizeDelta = new Vector2(600 * mCurrentHappiness * factor, 40);
        monkeyGiftImageHolder.sprite = monkeyImages[(int)((mCurrentHappiness / 100.0f) * 6)];

        if (giftsRemaining <= 0) {
            if (spanksRemaining <= 0) {
                mTurnOverListener(turnTotalDelta);
            }
            giftCanvas.SetActive(false);
            giveGiftButtonInSpankPanel.gameObject.SetActive(false);
            return;
        }
    }

    public void TurnOffGiftMark() {
        giftMark.gameObject.SetActive(false);
        giftRemainingText.text = "Gift (" + giftsRemaining + ")";
        giftRemainingTextGUI.text = giftsRemaining.ToString();
        giveGiftButton.interactable = true;
    }

    public void FinishTurn() {
        mTurnOverListener(turnTotalDelta);
        giftCanvas.SetActive(false);
        spankCanvas.SetActive(false);
        spankFinishTurnButton.gameObject.SetActive(false);
    }

    public void PlayerTurnOver() {

    }

    public void PlayerStrike() {
        mCurrentHappiness = 0;
        strikeCanvas.SetActive(true);
        mPlayer._playerGameData._livesLeft--;
        mPlayer.UpdatePlayerGameData();

        if (mPlayer._playerGameData._livesLeft <= 0) {
            PlayerLoss();
        } else {
            //strikeImage.sprite = strikeSprites[mPlayer._playerGameData._livesLeft];
            coinsText.text = (3 - mPlayer._playerGameData._livesLeft).ToString();
        }
        AudioManager.instance.PlayStrike();
    }

    public void PlayerLoss() {
        playerLost = true;
        defeatCanvas.SetActive(true);
        AudioManager.instance.PlayDefeat();
    }

    public void GetWinRewards(){
        if(!playerLost){
            winCanvas.SetActive(true);
            APIManager.sInstance.GetUserInventory(APIManager.sInstance.userID, GetInvCallback);
        }
    }

    private void GetInvCallback(Inventory inv_){
        int originalMonkeyPoints = inv_.monkeyPoints.amount;
        APIManager.sInstance.SetUserInventory(APIManager.sInstance.userID, EItems.monkeyPoints, originalMonkeyPoints + 25000, null);
    }

    public void ContinueAfterStrike() {
        strikeCanvas.SetActive(false);
        mTurnOverListener(turnTotalDelta);
    }

    public void ResetCanvas() {

    }

    public void ShowAIPlay(int aiTurn_) {
        CancelInvoke("HideAIPlay");
        string s = "";
        if (aiTurn_ > 0) {
            s += "Robomonkey \nrolled a " + aiTurn_ + ",\n" + "It spanked\n" + aiTurn_ + " times";
        } else {
            s += "Robomonkey \nrolled a 6" + ",\n" + "It gifted \n" + Mathf.Abs(aiTurn_) + " times";
        }

        aiPlayText.text = s;
        Invoke("HideAIPlay", aiPlayShowDelay);
    }

    public void HideAIPlay() {
        aiPlayText.text = "";
    }

}
