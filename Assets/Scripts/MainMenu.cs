﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public InputField username;
    public InputField password;
    public GameObject loginPanel;
    public GameObject gameModeSelectPanel;
    public GameObject shopPanel;
    public GameObject payDabalooneyPanel;
    public Image background;
    public Sprite loginBG;
    public Sprite gameModelSelectBG;
    public Text welcomeMessage;
    public Button payDabalooneyButton;
    public Text dabalooneyText;
    private int availableDabalooney;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void PlayVsFriends() {
        APIManager.sInstance.isAIMode = false;
        PayDabalooneyOpen();
    }

    public void PlayVSOthers() {
        APIManager.sInstance.isAIMode = false;
        PayDabalooneyOpen();
    }

    public void PlayVSRobomonkey() {
        APIManager.sInstance.isAIMode = true;
        PayDabalooneyOpen();
    }

    public void Signup() {
        Application.OpenURL("https://players.3monkey.games/signup/");
    }

    public void Login() {
        APIManager.sInstance.Login(username.text, password.text, LoginCallback);
    }

    public void PlayAsGuest() {
        loginPanel.SetActive(false);
        welcomeMessage.text = "Wecome Guest";
        welcomeMessage.gameObject.SetActive(true);
        background.sprite = gameModelSelectBG;
        gameModeSelectPanel.SetActive(true);
    }

    private void LoginCallback() {
        APIManager.sInstance.GetUserInfo(GetUsernameCallback);
    }

    private void GetUsernameCallback(UserInfo userInfo_) {
        loginPanel.SetActive(false);
        background.sprite = gameModelSelectBG;
        welcomeMessage.text = "Welcome " + userInfo_.username;
        welcomeMessage.gameObject.SetActive(true);
        gameModeSelectPanel.SetActive(true);
    }

    public void LogOut() {
        welcomeMessage.gameObject.SetActive(false);
        gameModeSelectPanel.SetActive(false);
        APIManager.sInstance.LogOut();
        background.sprite = loginBG;
        loginPanel.SetActive(true);
    }

    public void ShopOpen() {
        shopPanel.SetActive(true);
    }

    public void ShopClose() {
        shopPanel.SetActive(false);
    }

    public void PayDabalooneyOpen() {
        payDabalooneyPanel.SetActive(true);
        payDabalooneyButton.interactable = false;
        APIManager.sInstance.GetUserInventory(APIManager.sInstance.userID, DabalooneyCallback);
    }

    private void DabalooneyCallback(Inventory inventory_) {
        availableDabalooney = inventory_.dabalooney.amount;
        dabalooneyText.text = availableDabalooney.ToString();
        payDabalooneyButton.interactable = true;
    }

    public void PayDabalooneyClose() {
        payDabalooneyPanel.SetActive(false);
    }

    public void PayDabalooney() {
        if (availableDabalooney > 0) {
            APIManager.sInstance.SetUserInventory(APIManager.sInstance.userID, EItems.dabalooney, availableDabalooney - 1, null);
            SceneManager.LoadScene("GameScene");
        }
    }

}
