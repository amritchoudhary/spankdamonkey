﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LocalTurnHandler : NetworkBehaviour
{
    public int syncedInt = 0;
    public int playerID = 0;
    public int score = 0;
    private ServerManager mServerManager;
    public int currentHappiness = 0;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        if (isLocalPlayer) {
            GUIManager.instance.ShowStatusText("var: " + syncedInt);

            if (Input.GetKeyDown(KeyCode.C)) {
                CmdChangeVarFromClient();
            }
        }
    }

    public void UpdateClient(int syncInt_) {
        RpcChangeVarFromServer(syncInt_);
    }

    [ClientRpc]
    public void RpcChangeVarFromServer(int syncInt_) {
        syncedInt = syncInt_;
    }

    public void ChangeHappiness(int newHappiness_) {
        RpcChangeHappiness(newHappiness_);
    }

    [ClientRpc]
    public void RpcChangeHappiness(int newHappiness_) {
        currentHappiness = newHappiness_;
    }

    public void Init(ServerManager serverManager_, int playerID_) {
        mServerManager = serverManager_;
        RpcInit(playerID_);
    }

    [ClientRpc]
    private void RpcInit(int playerID_) {
        playerID = playerID_;
        if (isLocalPlayer) {
            GUIManager.instance.ShowReadyCanvas(true);
            GUIManager.instance.readyButton.onClick.AddListener(ClientReady);
        }
    }

    [Command]
    public void CmdChangeVarFromClient() {
        mServerManager.UpdateFromClient();
    }

    public void ClientReady() {
        CmdClientReady();
        GUIManager.instance.readyButton.onClick.RemoveAllListeners();
        GUIManager.instance.ShowReadyCanvas(false);
    }

    [Command]
    public void CmdClientReady() {
        mServerManager.ifPlayersReady[playerID] = true;
    }

    public override void OnStartServer() {
        ServerManager serverManager = FindObjectOfType<ServerManager>();
        if (serverManager != null) {
            serverManager.NewPlayer(this);
        }
    }

    public void GetTurn() {
        RpcGetTurn();
    }

    [ClientRpc]
    public void RpcGetTurn() {
        ActualGetTurn();
    }

    private void ActualGetTurn() {
        if (isLocalPlayer) { }
            //GUIManager.instance.StartTurn(TurnOverListener, currentHappiness);
    }

    public void TurnOverListener(int turnResponse_) {
        CmdTurnPlayed(turnResponse_);
    }

    [Command]
    public void CmdTurnPlayed(int i_) {
        TurnPlayed(i_);
    }

    public void TurnPlayed(int i_) {
        mServerManager.TurnPlayed(playerID, i_);
    }
}
