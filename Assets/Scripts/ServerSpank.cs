﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TBM;

public class ServerSpank : Server
{

    public int _perSpankFactor;

    public override void TurnPlayed(TurnReturnData turnReturnData_) {
        players[turnPlayerIndex]._playerGameData._score += turnReturnData_._value;
        players[turnPlayerIndex].UpdatePlayerGameData();

        int newValue = players[0]._commonData._value + turnReturnData_._value * _perSpankFactor;

        if(APIManager.sInstance.isAIMode){
            int aiTurn = UnityEngine.Random.Range(-5, 29);

            if(aiTurn > 0)
                aiTurn = aiTurn / 5;

            if(aiTurn == 0)
                aiTurn = 1;

            GUIManager.instance.ShowAIPlay(aiTurn);
            newValue += aiTurn * _perSpankFactor;
        }

        if (newValue >= 100)
            newValue = 0;
            
        if(newValue <= 0)
            newValue = 0;

        foreach (Player player in players) {
            player._commonData._value = newValue;
            player.UpdateCommonData();
        }

        turnPlayerIndex++;

        if (turnPlayerIndex >= players.Count) {
            turnPlayerIndex = 0;
            RoundOver();
        } else {
            GiveTurn();
        }
    }

}
