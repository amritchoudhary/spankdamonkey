﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LoginTest : MonoBehaviour
{

    private WWWForm form;
    private WWW download;
    string _url = "https://api.3monkey.games/A7KN5R";
    public int userID;

    // Use this for initialization
    void Start() {
		//SetInventory(3, 3, 1);
		SetInventory(3, 4, 1);
		//GetInventory(3);
    }

    private void TestApi() {
        form = new WWWForm();
        form.AddField("request", "test");

        StartCoroutine("SubmitForm", form);
    }

    private void Login(string username_, string password_) {
        form = new WWWForm();
        form.AddField("request", "login");
        form.AddField("user", username_);
        form.AddField("pass", password_);

        StartCoroutine("SubmitForm", form);
    }

    private void GetUser(int id_) {
        form = new WWWForm();
        form.AddField("request", "getUser");
        form.AddField("uid", id_);

        StartCoroutine("SubmitForm", form);
    }

    private void GetInventory(int id_) {
        form = new WWWForm();
        form.AddField("request", "getInv");
        form.AddField("uid", id_);

        StartCoroutine("SubmitForm", form);
    }

    private void SetInventory(int id_, int itemid_, int change_) {
        form = new WWWForm();
        form.AddField("request", "setInv");
        form.AddField("uid", id_);
        form.AddField("itemid", itemid_);
        form.AddField("change", change_);

        StartCoroutine("SubmitForm", form);
    }

    IEnumerator SubmitForm(WWWForm wwwForm) {
        download = new WWW(_url, wwwForm);
        yield return download;
        if (!string.IsNullOrEmpty(download.error)) {
            print("Error downloading: " + download.error);
        } else {
            Debug.Log("Result: " + download.text);
        }
    }

    private void ParseLogin(string loginString_) {
        string[] subStrings = loginString_.Split(':');
        int returnCode = int.Parse(subStrings[0]);
        string returnMessage = subStrings[1];
        userID = int.Parse(subStrings[2]);
    }

    // Update is called once per frame
    void Update() {

    }
}
