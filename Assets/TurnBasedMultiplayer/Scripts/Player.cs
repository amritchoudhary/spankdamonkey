﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace TBM
{
    public class Player : NetworkBehaviour
    {

        #region Player data and in-game data

        public PlayerInfo _playerInfo;

        public void UpdatePlayerInfo() {
            string playerInfoString = JsonUtility.ToJson(_playerInfo);

            if (isServer)
                RpcUpdatePlayerInfo(playerInfoString);
            else
                CmdUpdatePlayerInfo(playerInfoString);
        }

        [Command]
        protected void CmdUpdatePlayerInfo(string playerInfoString_) {
            _playerInfo = JsonUtility.FromJson<PlayerInfo>(playerInfoString_);
            RpcUpdatePlayerInfo(playerInfoString_);
        }

        [ClientRpc]
        protected void RpcUpdatePlayerInfo(string playerInfoString_) {
            _playerInfo = JsonUtility.FromJson<PlayerInfo>(playerInfoString_);
        }

        public PlayerGameData _playerGameData;

        public void UpdatePlayerGameData() {
            string playerGameDataString = JsonUtility.ToJson(_playerGameData);

            if (isServer)
                RpcUpdatePlayerGameData(playerGameDataString);
            else
                CmdUpdatePlayerGameData(playerGameDataString);
        }

        [Command]
        protected void CmdUpdatePlayerGameData(string playerGameDataString_) {
            _playerGameData = JsonUtility.FromJson<PlayerGameData>(playerGameDataString_);
            RpcUpdatePlayerGameData(playerGameDataString_);
        }

        [ClientRpc]
        protected void RpcUpdatePlayerGameData(string playerGameDataString_) {
            _playerGameData = JsonUtility.FromJson<PlayerGameData>(playerGameDataString_);
        }

        public CommonData _commonData;

        public void UpdateCommonData() {
            string commonDataString = JsonUtility.ToJson(_commonData);
            RpcUpdateCommonData(commonDataString);
        }

        [ClientRpc]
        protected void RpcUpdateCommonData(string commonDataString_) {
            _commonData = JsonUtility.FromJson<CommonData>(commonDataString_);
        }

        #endregion

        private Server mServer;
        protected TurnReturnData turnReturnData;

        public override void OnStartServer() {
            InitPlayer();
        }

        protected virtual void InitPlayer() {
            Debug.Log("Init Player");
            _playerInfo._playerName = "player";
            _playerInfo._ifPlayerReady = false;
            _playerInfo._playerID = AddPlayerToServer();

            _playerGameData._score = 0;
            _playerGameData._livesLeft = 3;

            UpdatePlayerInfo();
            UpdatePlayerGameData();
        }

        protected virtual int AddPlayerToServer() {
            Server server = FindObjectOfType<Server>();
            if (server != null) {
                return server.AddPlayer(this, ref mServer);
            }
            return 0;
        }

        public virtual void AskReady() {
            RpcAskReady();
        }

        [ClientRpc]
        public virtual void RpcAskReady() {

        }

        protected virtual void AskReadyListener() {
            _playerInfo._ifPlayerReady = true;
            UpdatePlayerInfo();
        }

        public virtual void GetTurn() {
            RpcGetTurn();
        }

        [ClientRpc]
        protected virtual void RpcGetTurn() {
            if (isLocalPlayer) {
                Debug.Log("Common Data " + _commonData._value);
                turnReturnData = new TurnReturnData();
                turnReturnData._value = Random.Range(0, 10);
            }
        }

        protected virtual void TurnOverListener() {
            string turnReturnDataString = JsonUtility.ToJson(turnReturnData);
            CmdTurnPlayed(turnReturnDataString);
        }

        protected virtual void TurnOverListener(int value_) {
            string turnReturnDataString = JsonUtility.ToJson(turnReturnData);
            CmdTurnPlayed(turnReturnDataString);
        }

        [Command]
        protected virtual void CmdTurnPlayed(string turnReturnDataString_) {
            TurnReturnData turnReturnData = JsonUtility.FromJson<TurnReturnData>(turnReturnDataString_);
            TurnPlayed(turnReturnData);
        }

        protected virtual void TurnPlayed(TurnReturnData turnReturnData_) {
            mServer.TurnPlayed(turnReturnData_);
        }

        // Use this for initialization
        void Start() {

        }

        // Update is called once per frame
        void Update() {

        }
    }
}
