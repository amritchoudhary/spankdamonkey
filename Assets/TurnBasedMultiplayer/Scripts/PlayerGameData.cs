﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TBM
{
    /// <summary>
    /// In game data like score, health
    /// </summary>
    [System.Serializable]
    public class PlayerGameData 
    {
        [SerializeField] public int _score;
        [SerializeField] public int _livesLeft;

        public PlayerGameData() {
            _score = 0;
            _livesLeft = 3;
        }

        public PlayerGameData(int score_, int livesLeft_) {
            _score = score_;
            _livesLeft = livesLeft_;
        }
    }
}
