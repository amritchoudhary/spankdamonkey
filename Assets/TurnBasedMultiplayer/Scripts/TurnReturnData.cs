﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TBM
{
    /// <summary>
    /// In game data returned every turn by players
    /// </summary>
    [System.Serializable]
    public class TurnReturnData
    {
        [SerializeField] public int _value;

        public TurnReturnData() {
            _value = 0;
        }

        public TurnReturnData(int value_) {
            _value = value_;
        }
    }
}
