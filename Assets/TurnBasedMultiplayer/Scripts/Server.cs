﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace TBM
{
    public class Server : NetworkBehaviour
    {
        public List<Player> players = new List<Player>();
        protected int turnPlayerIndex = 0;

        public virtual int AddPlayer(Player player_, ref Server server_) {
            players.Add(player_);
            server_ = this;
            return players.Count - 1;
        }
        
        public virtual void AskReady() {
            players.ForEach(X => X.AskReady());
        }

        public virtual void TryGameStart() {
            if (IfAllPlayersReady()) {
                GiveTurn();
            } else {
                Debug.Log("Not all ready");
            }
        }

        protected virtual bool IfAllPlayersReady() {
            foreach(Player player in players) {
                if (player._playerInfo._ifPlayerReady == false)
                    return false;
            }
            return true;
        }

        protected void GiveTurn() {
            players[turnPlayerIndex].GetTurn();
        }

        public virtual void TurnPlayed(TurnReturnData turnReturnData_) {
            Debug.Log(turnReturnData_._value + ", " + players[turnPlayerIndex]._playerInfo._playerID);
            players[turnPlayerIndex]._playerGameData._score += turnReturnData_._value;
            players[turnPlayerIndex].UpdatePlayerGameData();

            foreach(Player player in players) {
                player._commonData._value += turnReturnData_._value;
                player.UpdateCommonData();
            }

            turnPlayerIndex++;

            if (turnPlayerIndex >= players.Count) {
                turnPlayerIndex = 0;
                RoundOver();
            } else {
                GiveTurn();
            }
        }

        protected virtual void RoundOver() {
            GiveTurn();
        }
    }
}
