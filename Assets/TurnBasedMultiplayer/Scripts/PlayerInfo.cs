﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TBM
{
    /// <summary>
    /// player info like id, name etc. set before the game starts
    /// </summary>
    [System.Serializable]
    public class PlayerInfo
    {
        [SerializeField] public int _playerID;
        [SerializeField] public string _playerName;
        [SerializeField] public bool _ifPlayerReady;

        public PlayerInfo() {
            _playerID = 0;
            _playerName = "";
            _ifPlayerReady = false;
        }
    }
}
