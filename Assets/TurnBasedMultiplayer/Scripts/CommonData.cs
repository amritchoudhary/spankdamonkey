﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TBM
{
    /// <summary>
    /// In game data common to all players
    /// </summary>
    [System.Serializable]
    public class CommonData
    {
        [SerializeField] public int _value;

        public CommonData() {
            _value = 0;
        }

        public CommonData(int value_) {
            _value = value_;
        }
    }
}
